section .text

%include "lib.inc"

%define CONSTANTA_1 8

global find_word

find_word:
	; rdi -> null terminated string
	; rsi -> begin of the dict
	.loop:
		push rsi
		add rsi, CONSTANTA_1
		push rdi
		call string_equals
		pop rdi
		pop rsi
		test rax, rax
		jnz .success
		mov rsi, [rsi]
		test rsi, rsi
		jnz .loop
	.end:
		xor rax, rax
		ret
	.success:
		mov rax, rsi
		ret
