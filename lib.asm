section .text

global print_int
global string_equals
global read_char
global read_word
global exit
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_uint
global parse_uint
global string_copy
global parse_int
global read_line

%define newline_char 0xA


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret
        
        
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: ; rdi -> strbegin
    call string_length ; rax -> strlen
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret
    
    
; Принимает код символа и выводит его в stdout
print_char: ; rdi = arg1
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret
    
    
; Переводит строку
print_newline:
    mov rdi, 10
    jmp print_char
    
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    xor rax, rax 
    mov r10, 10
    mov r11, rsp
    mov rax, rdi
    push 0
    .cycle:
        xor rdx, rdx
        div r10 
        add rdx, 0x30
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        je .end
        jmp .cycle
    .end:
        mov rdi, rsp
        push r11
        call print_string
        pop r11
        mov rsp, r11
        ret
        
        
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    .print_pos:
        cmp rdi, 0
        jl .print_neg
        call print_uint
        ret
    .print_neg:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret
    
    
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: ;rcx, rdx, cl, dl - empty regs
    xor rax, rax
	xor rcx, rcx
	xor rdx, rdx
	.loop:
    	mov cl, [rdi] ; put 1st symbol of 1st string
    	mov dl, [rsi] ; put 2nd symbol of 2nd string
    	cmp cl, dl
    	jne .clr_rax ; return value
    	inc rdi
    	inc rsi
    	test cl, cl ; if cl is 0 terminator
    	jnz .loop
    	mov rax, 1
    	jmp .end
    .clr_rax:
	    xor rax, rax
    .end:
        ret
    
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока ?????
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .end
    mov al, [rsp]
    .end:
        inc rsp
        ret
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx  
    cmp rsi, 0 ;buffer size is not 0
    je .error
    .loop:
        push rdi ; push all caller saved registers
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        cmp rax, ' ' ; space
        je .is_blank
        cmp rax, '	' ; tab
        je .is_blank
        cmp rax, '\n'
        je .is_blank
        jmp .save_char
    .is_blank:
        cmp rdx, 0 ; number of already read symbols
        je .loop
        jmp .end
    .save_char:
        cmp rsi, rdx ; word is bigger than buffer
        je .error
        mov [rdi + rdx], al ; lower 8bit rax
        cmp rax, 0
        je .end
        inc rdx
        jmp .loop
    .error:
        xor rax, rax
        xor rdx, rdx
        ret
    .end: 
        mov rax, rdi
        ret

read_line:
	xor rdx, rdx
	cmp rsi, 0
	je .error
	.loop:
		push rdi
		push rsi
		push rdx
		call read_char
		pop rdx
		pop rsi
		pop rdi
		cmp rdx, 0
		jne .process_char
		cmp al, ' '
		je .loop
		cmp al, '	'
		je .loop
		cmp al, newline_char
		je .loop
	.process_char:
		cmp rsi, rdx
		je .error
		cmp al, newline_char
		jne .save
		xor rax, rax
	.save:
		mov [rdi + rdx], al
		cmp rax, 0
		je .success
		inc rdx
		jmp .loop
	.error:
		xor rax, rax
		xor rdx, rdx
		ret
	.success: 
		mov rax, rdi
		ret
        
        
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax - число; в rdx - его длину в символах
; rdx = 0 если число прочитать не удалось ?????
parse_uint:
    xor rax, rax
	xor rdx, rdx
    .loop:
    	cmp byte[rdi + rdx], '0'
    	jl .end
    	cmp byte[rdi + rdx], '9'
    	jg .end
    	push rdx
    	mov rdx, 10
    	mul rdx
    	pop rdx
    	xor r10, r10
    	mov r10b, byte[rdi + rdx]
    	add rax, r10
    	sub rax, '0'
    	inc rdx
    	jmp .loop
    .end:
        ret
        

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte[rdi], '-'
	je .neg
	jmp parse_uint
    .neg:
    	inc rdi
    	call parse_uint
    	inc rdx
    	neg rax
    ret
	
	
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rdx, rax ; if strlen bigger than buffer
        je .error
        mov cl, [rdi + rax] ; cl = rcx lower 8bits
        mov [rsi + rax], cl
        inc rax
        cmp cl, 0
        je .end
        jmp .loop
    .error:
        xor rax, rax
    .end:
        ret

