ASM=nasm
ASMFLAGS=-f elf64


program: main.o dict.o lib.o
	ld -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm words.inc lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean
clean:
	rm *.o program
