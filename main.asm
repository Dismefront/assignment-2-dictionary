section .text

%include "lib.inc"
%include "words.inc"
%define CONSTANTA_2
extern find_word

global _start

section .bss
buffer: times 256 db 0

section .rodata
msg_not_found: db "Could not find the key", 0
msg_long_key: db "The string is too long", 0

section .text

_start:
	mov rdi, buffer
	mov rsi, CONSTANTA_2
	call read_line
	test rax, rax
	jz .buffer_overflow
	
	push rdx
	mov rdi, rax
	mov rsi, nxt
	call find_word
	pop rdx
	test rax, rax
	jz .err
	
	add rax, 8
	add rax, rdx
	inc rax
	mov rdi, rax
	
	jmp .end

	.buffer_overflow:
		mov rdi, msg_long_key
		jmp .end
	.err:
		mov rdi, msg_not_found
	.end:
		call print_string
		call print_newline
		xor rdi, rdi
		call exit
	
